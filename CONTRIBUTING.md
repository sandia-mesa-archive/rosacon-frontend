# Contributing
Thank you for considering contributing to the Rosacon frontend.

You can contribute in the following ways.

- Finding and reporting bugs
- Translating the Rosacon frontend's interface into various languages
- Contributing code to Rosacon frontend by fixing bugs or implementing features
- Improving the documentation

## Bug Reports and Feature Requests
Bug reports and feature suggestions can be submitted to the Gitea Issues tracker for Rosacon Frontend. Please make sure you're not submitting duplicates, and that a similar request or report has not already been resolved or rejected in the past using the search function. Also, please use clear and concise titles.