# Rosacon Frontend

This will be a user and comment frontend that may eventually be used to power the Sandia Mesa websites. Not ready yet for production use.

## Copyright
(C) 2020 Sandia Mesa Animation Studios and other contributors (see [AUTHORS.md](AUTHORS.md))

Rosacon Frontend is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.