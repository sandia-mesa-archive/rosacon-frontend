# Authors
Rosacon Frontend is available on Sandia Mesa's self-hosted [Gitea](https://code.sandiamesa.com/sandiamesa/rosacon-frontend) and is provided thanks to the work of the following contributors:

* Sean King - <sean.king@sandiamesa.com>