<?php
	/**
	 *
	 * This powers the menu for managing the comments frontend.
	 *
	 * @package Rosacon_Frontend
	 * @since 1.0
	 */
	
	function rosacon_admin_comment_options() {
		
		if ( !current_user_can( 'manage_options' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}
		echo '<div class="wrap">';
		echo '<p>Here is where the comment frontend options would go.</p>';
		echo '</div>';
	}