<?php
	/**
	 *
	 * This indexes the includes for and initializes the admin menus.
	 *
	 * @package Rosacon_Frontend
	 * @since 1.0
	 */
	 
	// Includes
	require_once( dirname(__FILE__) . '/comments.php' );
	require_once( dirname(__FILE__) . '/users.php' );
	// Initialize Rosacon Frontend menus
	function rosacon_menus() {
		add_menu_page( 'Rosacon Frontend Comments', 'Rosacon Frontend', 'manage_options', 'rosacon_frontend/comments.php', 'rosacon_admin_comment_options' );
		
		add_submenu_page( 'rosacon_frontend/comments.php', 'Rosacon Frontend Comments', 'Comments', 'manage_options', 'rosacon_frontend/comments.php', 'rosacon_admin_comment_options' );
		add_submenu_page( 'rosacon_frontend/comments.php', 'Rosacon Frontend Users', 'Users', 'manage_options', 'rosacon_frontend/users.php', 'rosacon_admin_user_options' );
	}

	add_action( 'admin_menu', 'rosacon_menus' );