<?php
  /**
   *
   * Plugin Name: Rosacon Frontend
   * Plugin URI: https://code.sandiamesa.com/sandiamesa/rosacon-frontend
   * Description: A more minimalistic approach to creating a user-friendly comment, user profile, login, and registration interfaces.
   * Version: 1.0
   * Requires at least: TBD
   * Requires PHP: TBD
   * Author: Sandia Mesa Animation Studios
   * Author URI: https://sandiamesa.com
   * License: GPL v3 or later
   * License URI: https://www.gnu.org/licenses/gpl-3.0.html
   * Text Domain: rosacon-frontend
   */
   
   // You shall not access me directly.
   if (!defined( 'ABSPATH' )) {
	   die();
   }
   
   // Include admin options for the admin interface.
   if ( is_admin() ) {
	   require_once( dirname(__FILE__) . '/admin/index.php');
   }